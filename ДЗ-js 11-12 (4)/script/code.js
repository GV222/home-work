
let offset = 0;
let timer;

function autoSlider () {
    timer = setTimeout(sliderNext, 3000)
}


function sliderNext() {
    let sliderLine = document.querySelector('.slider-line');
    offset = offset + 200;
    if (offset > 800) {
        offset = 0;
    }
    sliderLine.style.left = -offset + 'px';
    autoSlider();
}
   

autoSlider(); 